import feedparser
import json
import requests
from flask import Flask
from flask import render_template, request
from flask_bootstrap import Bootstrap
from flask_wtf import Form
from wtforms import StringField, SubmitField
from wtforms.validators import Required

app = Flask(__name__)
bootstrap = Bootstrap(app)
RSS_FEEDS ={
    'bbc': "http://feeds.bbci.co.uk/news/rss.xml",
    'cnn': "http://rss.cnn.com/rss/edition.rss",
    'iol': "http://classic.iol.co.za/cmlink/1.640",
    'fox': "http://feeds.foxnews.com/foxnews/latest"
}


WEATHER_URL = 'http://api.openweathermap.org/data/2.5/' \
        'weather?q={}&units=metric&appid=898c1379483b86b10d8c34b45f0dac34'
        
CURRENCY_URL = 'https://openexchangerates.org/api' \
        '/latest.json?app_id=d9b0a84297944d24a0d0f571ecd81b7c'

DEFAULTS = {
    'publication': 'bbc',
    'city': 'London, UK',
    'currency_from': 'EUR',
    'currency_to': 'USD',
}




@app.route('/', methods=['GET', 'POST'])
def home():
    publication = request.args.get("publication")
    if not publication:
        publication = DEFAULTS['publication']
    articles = get_news(publication)
    # get city weather
    city = request.args.get('city')
    if not city:
        city = DEFAULTS['city']
    weather = get_weather(city)
    currency_from = request.args.get("currency_from")
    if not currency_from:
        currency_from=DEFAULTS['currency_from']
    currency_to = request.args.get("currency_to")
    if not currency_to:
        currency_to = DEFAULTS['currency_to']
    rate = get_rates(currency_from, currency_to)
    return render_template('home.html', articles=articles, weather=weather,
                    currency_from=currency_from, currency_to = currency_to, rate = rate
    )


def get_news(query):
    if not query or query.lower() not in RSS_FEEDS:
        publication = DEFAULTS['publication']
    else:
        publication = query.lower()
        
    feed = feedparser.parse(RSS_FEEDS[publication])
    #weather = get_weather("Karlsruhe, DE")
    articles = feed['entries']
    return articles
    #


def get_weather(query):
    api_url = WEATHER_URL
    search_url=api_url.format(query)
    data = requests.get(search_url)
    parsed = json.loads(data.text)
    weather = None
    if parsed.get("weather"):
        weather = {
            "description": parsed["weather"][0]['description'],
            "temperature": parsed["main"]["temp"],
            "temp_min": parsed["main"]["temp_min"],
            "temp_max": parsed["main"]["temp_max"],
            "city": parsed["name"],
            "country": parsed['sys']['country']
        }
    return weather


def get_rates(frm, to):
    data = requests.get(CURRENCY_URL)
    parsed = json.loads(data.text).get('rates')
    frm_rate = parsed.get(frm.upper())
    to_rate = parsed.get(to.upper())
    return to_rate/frm_rate
    



if __name__=='__main__':
    app.run(port=5000, debug=True)